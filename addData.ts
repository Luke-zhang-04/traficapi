#!/usr/bin/env -S deno run --allow-net
// Run with deno

const randInt = (min: number, max: number) =>
    Math.round(Math.random() * (max - min) + min),
  outputPromiseResults = (responses: Response[]) => {
    console.log("Checking response results");

    for (const [index, response] of responses.entries()) {
      console.log(response.status, response.statusText);

      if (response.status !== 200) {
        throw new Error(
          `Status returned ${response.status}. Iteration ${index}.`,
        );
      }
    }
  };

function* randomSpeeds(roadSpeed: number): Generator<number, void, void> {
  for (let i = 0; i < randInt(5, 10); i++) {
    const res = randInt(0, 20);

    /**
     * If res was smaller than 1, yield a speed less than the speedlimit
     * Otherswise, yeild a speed the same or highter than the speed limit
     */
    yield res > 1 ? roadSpeed + randInt(0, 10) : roadSpeed - randInt(1, 10);
  }
}

const url = "http://127.0.0.1:3333/",
  speedLimits = [40, 50, 60, 70, 80, 90, 100],
  roads: Promise<Response>[] = []; // Avoid awaiting in loops

for (const speed_limit of speedLimits) {
  roads.push(fetch(
    //sends request to routes.js
    `${url}newRoad`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      referrerPolicy: "no-referrer",
      body: JSON.stringify({
        speed_limit,
      }),
    },
  ));
}

interface RoadIds {
  id: number;
  speed: number;
}

const roadIds = speedLimits.map((
    speed,
    id,
  ): RoadIds => ({ speed, id })
  ),
  cars: Promise<Response>[] = [];

for (const road of roadIds) {
  for (const speed of randomSpeeds(road.speed)) {
    cars.push(fetch(
      `${url}newCar`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        referrerPolicy: "no-referrer",
        body: JSON.stringify({
          speed,
          road_id: road.id + 1,
        }),
      },
    ));
  }
}

Promise.all(roads).then(outputPromiseResults)
  .catch((err: Error) => {
    console.log(err.message);
  });

Promise.all(cars).then(outputPromiseResults)
  .catch((err: Error) => {
    console.log(err.message);
  });
