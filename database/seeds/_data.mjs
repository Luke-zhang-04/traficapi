export const speedLimits = [40, 50, 60, 70, 80, 90, 100];

const roads = {};

for (const [index, val] of speedLimits.entries()) {
  roads[index + 1] = val;
}

export {
  roads,
};
