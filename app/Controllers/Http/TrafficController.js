"use strict";

/** @type {typeof import("../../Models/Road")} */
const RoadModel = use("App/Models/Road");

/** @type {typeof import("../../Models/Car")} */
const CarModel = use("App/Models/Car");

class TrafficController {
  /**
   * Creates a new car
   * @param {Object.<string, any>} param0
   * @param {Request} param0.request
   * @returns {Promise.<typeof import('@adonisjs/lucid/src/Lucid/Model')>} car model
   */
  newCar = async ({ request }) => {
    const body = request.only(["speed", "road_id"]);
    const car = await CarModel.create(body);

    return car;
  };

  /**
   * Creates a new road
   * @public
   * @param {Object.<string, any>} param0
   * @param {Request} param0.request
   * @returns {Promise.<typeof import('@adonisjs/lucid/src/Lucid/Model')>} road model
   */
  newRoad = async ({ request }) => {
    const body = request.only(["speed_limit"]);
    const road = await RoadModel.create(body);

    return road;
  };

  /**
   * Check if a road has traffic
   * @param {Object.<string, any>} param0
   * @param {Object.<string, string>} param0.params
   * @returns {boolean | void} if traffic exists
   */
  hasTraffic = async ({ params }) => {
    const road = await RoadModel.findOrFail(params.id);
    const cars = (await road.cars()).rows; // Gets all cars within the road

    // Returns whether or not the number of cars below the speed limit is greater than 0
    return cars?.filter((car) => car.speed < road.speed_limit).length > 0;
  };

  /**
   * Check if a road has traffic
   * @param {Object.<string, any>} param0
   * @param {Object.<string, string>} param0.params
   * @returns {Object.<string, typeof import('@adonisjs/lucid/src/Lucid/Model') | Object.<string, Object>}
   * road info and it's cars
   */
  indexRoad = async ({ params }) => {
    const road = await RoadModel.findOrFail(Number(params.id));
    const cars = (await road.cars()).rows; // Gets all cars within the road

    return {
      road,
      cars,
    };
  };

  /**
   * Returns all the roads
   * @returns {Object.<number, Object.<string, number | string>>} all roads
   */
  getRoads = async () => await RoadModel.all();

  /**
   * Returns all the cars
   * @returns {Object.<number, Object.<string, number | string>>} all cars
   */
  getCars = async () => await CarModel.all();
}

module.exports = TrafficController;
